import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import componentsRouter from './modules/components'
import chartsRouter from './modules/charts'
import tableRouter from './modules/table'
import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
    }]
},
{
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
},
{
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
},
{
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
},
{
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
},
{
    path: '/',
    component: Layout,
    redirect: '/dashboard/index',
    hidden: true,
    children: [{
        path: '/dashboard/index',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard1',
        meta: {
            title: '首页',
            icon: 'dashboard'
        } // , affix: true
    },

    {
        path: '/dashboard/printing',
        name: 'Printing',
        component: () => import('@/views/dashboard/printing'),
        meta: {
            title: '合格证标签打印',
            icon: ''
        }
    },
    {
        path: '/dashboard/scancode',
        name: 'Scancode',
        component: () => import('@/views/dashboard/scancode'),
        meta: {
            title: '扫码',
            icon: ''
        }
    },
    {
        path: '/dashboard/edituser',
        name: 'Edituser',
        hidden: true,
        component: () => import('@/views/dashboard/edituser'),
        meta: {
            title: '账户信息',
            icon: ''
        }
    },
    {
        path: '/dashboard/collectdata',
        name: 'Collectdata',
        hidden: true,
        component: () => import('@/views/dashboard/collectdata'),
        meta: {
            title: '收集数据订单详情',
            icon: ''
        }
    },
    {
        path: '/dashboard/transfer',
        name: 'Transfer',
        hidden: true,
        component: () => import('@/views/dashboard/transfer'),
        meta: {
            title: '转账审核',
            icon: ''
        }
    },
    {
        path: '/dashboard/examine',
        name: 'Examine',
        hidden: true,
        component: () => import('@/views/dashboard/examine'),
        meta: {
            title: '会员审核',
            icon: ''
        }
    },
    {
        path: '/dashboard/product',
        name: 'Product',
        hidden: true,
        component: () => import('@/views/dashboard/product'),
        meta: {
            title: '成品仓库转移审核',
            icon: ''
        }
    },
    {
        path: '/dashboard/material',
        name: 'Material',
        hidden: true,
        component: () => import('@/views/dashboard/material'),
        meta: {
            title: '原材料仓库转移审核',
            icon: ''
        }
    }
    ]
},

{
    path: '/setting',
    component: Layout,
    redirect: '/setting/basicset',
    name: 'Setting',
    meta: {
        title: '设置',
        icon: ''
    },
    children: [{
        path: '/setting/basicset',
        name: 'Basicset',
        component: () => import('@/views/setting/basicset'),
        meta: {
            title: '基本设置',
            icon: ''
        }
    },
    {
        path: '/setting/logisttemplate/index',
        name: 'LogisttIndex',
        component: () => import('@/views/setting/logisttemplate/index'),
        meta: {
            title: '物流模板',
            icon: ''
        }
    },
    {
        path: '/setting/logisttemplate/create',
        name: 'LogistCreate',
        component: () => import('@/views/setting/logisttemplate/create'),
        hidden: true,
        meta: {
            title: '新建物流模板',
            icon: ''
        }
    },
    {
        path: '/setting/deliveAddress/index',
        name: 'DeladdressIndex',
        component: () => import('@/views/setting/deliveAddress/index'),
        meta: {
            title: '发货地址',
            icon: ''
        }
    },
    {
        path: '/setting/deliveAddress/create',
        name: 'DeladdressCreate1',
        component: () => import('@/views/setting/deliveAddress/create'),
        hidden: true,
        meta: {
            title: '新建地址',
            icon: ''
        }
    },
    {
        path: '/setting/deliveAddress/edit',
        name: 'DeladdressCreate2',
        component: () => import('@/views/setting/deliveAddress/edit'),
        hidden: true,
        meta: {
            title: '修改地址',
            icon: ''
        }
    },
    {
        path: '/setting/deliveList/index',
        name: 'DeliveIndex',
        component: () => import('@/views/setting/deliveList/index'),
        meta: {
            title: '物流列表',
            icon: ''
        }
    },
    {
        path: '/setting/deliveList/create',
        name: 'DeliveCreate1',
        component: () => import('@/views/setting/deliveList/create'),
        hidden: true,
        meta: {
            title: '添加列表',
            icon: ''
        }
    },
    {
        path: '/setting/deliveList/edit',
        name: 'DeliveCreate2',
        component: () => import('@/views/setting/deliveList/edit'),
        hidden: true,
        meta: {
            title: '修改列表',
            icon: ''
        }
    },
    {
        path: '/setting/freightList/index',
        name: 'FreightIndex',
        component: () => import('@/views/setting/freightList/index'),
        meta: {
            title: '运费列表',
            icon: ''
        }
    },
    {
        path: '/setting/freightList/create',
        name: 'FreightCreate',
        component: () => import('@/views/setting/freightList/create'),
        hidden: true,
        meta: {
            title: '添加列表',
            icon: ''
        }
    },
    {
        path: '/setting/freightList/edit',
        name: 'FreightEdit',
        component: () => import('@/views/setting/freightList/edit'),
        hidden: true,
        meta: {
            title: '修改列表',
            icon: ''
        }
    },
    {
        path: '/setting/logList/index',
        name: 'LogList',
        component: () => import('@/views/setting/logList/index'),
        meta: {
            title: '日志列表',
            icon: ''
        }
    },
    {
        path: '/serveTest/index',
        name: 'ServeTest',
        component: () => import('@/views/setting/serveTest/index'),
        meta: {
            title: '服务测试',
            icon: ''
        }
    },
    {
        path: '/setting/instruct/index',
        name: 'Instruct1',
        component: () => import('@/views/setting/instruct/index'),
        meta: {
            title: '使用说明',
            icon: ''
        }
    }
    ]
},
{
    path: '/userManage',
    component: Layout,
    redirect: '/userManage/manageSet',
    name: 'userManage',
    meta: {
        title: '用户管理',
        icon: ''
    },
    children: [{
        path: '/userManage/manageSet/index',
        name: 'ManageSet1',
        component: () => import('@/views/userManage/manageSet/index'),
        meta: {
            title: '管理组设置',
            icon: ''
        }
    },
    {
        path: '/userManage/manageSet/create',
        name: 'ManageSet2',
        component: () => import('@/views/userManage/manageSet/create'),
        hidden: true,
        meta: {
            title: '添加管理组',
            icon: ''
        }
    },
    {
        path: '/userManage/manageSet/edit',
        name: 'ManageSet2edit',
        component: () => import('@/views/userManage/manageSet/edit'),
        hidden: true,
        meta: {
            title: '修改管理组',
            icon: ''
        }
    },
    {
        path: '/userManage/memberList/index',
        name: 'MemberList1',
        component: () => import('@/views/userManage/memberList/index'),
        meta: {
            title: '会员列表',
            icon: ''
        }
    },
    {
        path: '/userManage/memberList/create',
        name: 'MemberList2',
        component: () => import('@/views/userManage/memberList/create'),
        hidden: true,
        meta: {
            title: '添加会员',
            icon: ''
        }
    },
    {
        path: '/userManage/memberList/edit',
        name: 'MemberList3',
        component: () => import('@/views/userManage/memberList/edit'),
        hidden: true,
        meta: {
            title: '修改会员',
            icon: ''
        }
    },
    {
        path: '/userManage/memberList/examine',
        name: 'MemberList4',
        component: () => import('@/views/userManage/memberList/examine'),
        hidden: true,
        meta: {
            title: '会员审核',
            icon: ''
        }
    },
    {
        path: '/userManage/memberList/resetPwd',
        name: 'MemberList5',
        component: () => import('@/views/userManage/memberList/resetPwd'),
        hidden: true,
        meta: {
            title: '重置密码',
            icon: ''
        }
    },
    {
        path: '/userManage/employeeList/index',
        name: 'EmployeeList1',
        component: () => import('@/views/userManage/employeeList/index'),
        meta: {
            title: '员工列表',
            icon: ''
        }
    },
    {
        path: '/userManage/employeeList/create',
        name: 'EmployeeList2',
        component: () => import('@/views/userManage/employeeList/create'),
        hidden: true,
        meta: {
            title: '添加员工',
            icon: ''
        }
    },
    {
        path: '/userManage/employeeList/edit',
        name: 'EmployeeListedit2',
        component: () => import('@/views/userManage/employeeList/edit'),
        hidden: true,
        meta: {
            title: '修改员工',
            icon: ''
        }
    },
    {
        path: '/userManage/xuexiao/index',
        name: 'xuexiao',
        component: () => import('@/views/userManage/xuexiao/index'),
        meta: {
            title: '学校列表',
            icon: ''
        }
    },
    {
        path: '/userManage/xuexiao/create',
        name: 'xuexiaoadd',
        component: () => import('@/views/userManage/xuexiao/create'),
        hidden: true,
        meta: {
            title: '添加学校',
            icon: ''
        }
    },
    {
        path: '/userManage/xuexiao/edit',
        name: 'xuexiaoedit',
        component: () => import('@/views/userManage/xuexiao/edit'),
        hidden: true,
        meta: {
            title: '修改学校',
            icon: ''
        }
    },
    {
        path: '/userManage/materialSup/index',
        name: 'MaterialSup1',
        component: () => import('@/views/userManage/materialSup/index'),
        meta: {
            title: '原材料供应商',
            icon: ''
        }
    },
    {
        path: '/userManage/materialSup/create',
        name: 'MaterialSup2',
        component: () => import('@/views/userManage/materialSup/create'),
        hidden: true,
        meta: {
            title: '添加原材料供应商',
            icon: ''
        }
    },
    {
        path: '/userManage/materialSup/edit',
        name: 'MaterialSup3',
        component: () => import('@/views/userManage/materialSup/edit'),
        hidden: true,
        meta: {
            title: '修改原材料供应商',
            icon: ''
        }
    },
    {
        path: '/userManage/instruct/index',
        name: 'Instruct2',
        component: () => import('@/views/userManage/instruct/index'),
        meta: {
            title: '使用说明',
            icon: ''
        }
    }
    ]
},
{
    path: '/warehouse',
    component: Layout,
    redirect: '/warehouse/warehouseList',
    name: 'warehouse',
    meta: {
        title: '仓库',
        icon: ''
    },
    children: [{
        path: '/warehouse/warehouseList/index',
        name: 'WarehouseList1',
        component: () => import('@/views/warehouse/warehouseList/index'),
        meta: {
            title: '仓库列表',
            icon: ''
        }
    },
    {
        path: '/warehouse/warehouseList/edit',
        name: 'WarehouseList3',
        component: () => import('@/views/warehouse/warehouseList/edit'),
        hidden: true,
        meta: {
            title: '修改仓库列表',
            icon: ''
        }
    },
    {
        path: '/warehouse/warehouseList/create',
        name: 'WarehouseList2',
        component: () => import('@/views/warehouse/warehouseList/create'),
        hidden: true,
        meta: {
            title: '添加仓库列表',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishPro/index',
        name: 'FinishPro1',
        component: () => import('@/views/warehouse/finishPro/index'),
        meta: {
            title: '成品仓库转移列表',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishPro/create',
        name: 'FinishPro2',
        component: () => import('@/views/warehouse/finishPro/create'),
        hidden: true,
        meta: {
            title: '添加成品仓库转移',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishPro/edit',
        name: 'FinishPro3',
        component: () => import('@/views/warehouse/finishPro/edit'),
        hidden: true,
        meta: {
            title: '编辑成品仓库转移',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishPro/form',
        name: 'FinishPro4',
        component: () => import('@/views/warehouse/finishPro/form'),
        hidden: true,
        meta: {
            title: '成品仓库转移单',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishmat/index',
        name: 'Finishmat1',
        component: () => import('@/views/warehouse/finishmat/index'),
        meta: {
            title: '原材料仓库转移列表',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishmat/create',
        name: 'Finishmat2',
        component: () => import('@/views/warehouse/finishmat/create'),
        hidden: true,
        meta: {
            title: '添加原材料仓库转移',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishmat/edit',
        name: 'Finishmat3',
        component: () => import('@/views/warehouse/finishmat/edit'),
        hidden: true,
        meta: {
            title: '编辑原材料仓库转移',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishmat/review',
        name: 'Finishmat4',
        component: () => import('@/views/warehouse/finishmat/review'),
        hidden: true,
        meta: {
            title: '原材料仓库转移审核',
            icon: ''
        }
    },
    {
        path: '/warehouse/finishmat/form',
        name: 'Finishmat5',
        component: () => import('@/views/warehouse/finishmat/form'),
        hidden: true,
        meta: {
            title: '原材料转移单',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockpro/index',
        name: 'Stockpro1',
        component: () => import('@/views/warehouse/stockpro/index'),
        meta: {
            title: '成品盘存表',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockpro/create',
        name: 'Stockpro2',
        component: () => import('@/views/warehouse/stockpro/create'),
        hidden: true,
        meta: {
            title: '添加成品盘存表',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockpro/edit',
        name: 'Stockpro3',
        component: () => import('@/views/warehouse/stockpro/edit'),
        hidden: true,
        meta: {
            title: '编辑成品盘存',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockpro/form',
        name: 'Stockpro4',
        component: () => import('@/views/warehouse/stockpro/form'),
        hidden: true,
        meta: {
            title: '成品盘存单',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockmat/index',
        name: 'Stockmat1',
        component: () => import('@/views/warehouse/stockmat/index'),
        meta: {
            title: '原材料盘存表',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockmat/create',
        name: 'Stockmat2',
        component: () => import('@/views/warehouse/stockmat/create'),
        hidden: true,
        meta: {
            title: '添加原材料盘存表',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockmat/edit',
        name: 'Stockmat3',
        component: () => import('@/views/warehouse/stockmat/edit'),
        hidden: true,
        meta: {
            title: '修改原材料盘存表',
            icon: ''
        }
    },
    {
        path: '/warehouse/stockmat/form',
        name: 'Stockmat4',
        component: () => import('@/views/warehouse/stockmat/form'),
        hidden: true,
        meta: {
            title: '原材料盘存单',
            icon: ''
        }
    },
    {
        path: '/warehouse/instruct/index',
        name: 'Instruct3',
        component: () => import('@/views/warehouse/instruct/index'),
        meta: {
            title: '使用说明',
            icon: ''
        }
    }
    ]
},
{
    path: '/factory',
    component: Layout,
    redirect: '/factory/factoryList',
    name: 'factory',
    meta: {
        title: '工厂',
        icon: ''
    },
    children: [/* {
        path: '/factory/factoryList/index',
        name: 'FactoryList1',
        component: () => import('@/views/factory/factoryList/index'),
        meta: {
            title: '工厂列表',
            icon: ''
        }
    },
    {
        path: '/factory/factoryList/create',
        name: 'FactoryList2',
        component: () => import('@/views/factory/factoryList/create'),
        hidden: true,
        meta: {
            title: '添加工厂',
            icon: ''
        }
    },
    {
        path: '/factory/factoryList/edit',
        name: 'FactoryList3',
        component: () => import('@/views/factory/factoryList/edit'),
        hidden: true,
        meta: {
            title: '编辑工厂',
            icon: ''
        }
    },
    {
        path: '/factory/factoryList/process',
        name: 'FactoryList4',
        component: () => import('@/views/factory/factoryList/process'),
        hidden: true,
        meta: {
            title: '进度查询',
            icon: ''
        }
    },*/
        {
            path: '/factory/outworkshop/index',
            name: 'Outworkshop1',
            component: () => import('@/views/factory/outworkshop/index'),
            meta: {
                title: '外发加工厂',
                icon: ''
            }
        },
        {
            path: '/factory/outworkshop/create',
            name: 'Outworkshop2',
            component: () => import('@/views/factory/outworkshop/create'),
            hidden: true,
            meta: {
                title: '添加原材料外发车间',
                icon: ''
            }
        },
        {
            path: '/factory/outworkshop/edit',
            name: 'Outworkshop3',
            component: () => import('@/views/factory/outworkshop/edit'),
            hidden: true,
            meta: {
                title: '编辑原材料外发车间',
                icon: ''
            }
        },
        {
            path: '/factory/outworkshop/process',
            name: 'Outworkshop4',
            component: () => import('@/views/factory/outworkshop/process'),
            hidden: true,
            meta: {
                title: '进度查询',
                icon: ''
            }
        },
        {
            path: '/factory/calibrate/index',
            name: 'Calibrate1',
            component: () => import('@/views/factory/calibrate/index'),
            meta: {
                title: '校标厂列表',
                icon: ''
            }
        },
        {
            path: '/factory/calibrate/create',
            name: 'Calibrate2',
            component: () => import('@/views/factory/calibrate/create'),
            hidden: true,
            meta: {
                title: '添加校标厂',
                icon: ''
            }
        },
        {
            path: '/factory/calibrate/edit',
            name: 'Calibrate3',
            component: () => import('@/views/factory/calibrate/edit'),
            hidden: true,
            meta: {
                title: '编辑校标厂',
                icon: ''
            }
        },
        {
            path: '/factory/calibrate/process',
            name: 'Calibrate4',
            component: () => import('@/views/factory/calibrate/process'),
            hidden: true,
            meta: {
                title: '进度查询',
                icon: ''
            }
        },
        {
            path: '/factory/instruct/index',
            name: 'Instruct4',
            component: () => import('@/views/factory/instruct/index'),
            meta: {
                title: '使用说明',
                icon: ''
            }
        }
    ]
},
{
    path: '/product',
    component: Layout,
    redirect: '/product/productList',
    name: 'product',
    meta: {
        title: '产品',
        icon: ''
    },
    children: [{
        path: '/product/productList/index',
        name: 'productList1',
        component: () => import('@/views/product/productList/index'),
        meta: {
            title: '产品列表',
            icon: ''
        }
    },
    {
        // path: '/product/productList/create/index',
        path: '/product/productList/create',
        name: 'productList2',
        // component: () => import('@/views/product/productList/create/index'),
        component: () => import('@/views/product/productList/create'),
        hidden: true,
        meta: {
            title: '添加产品',
            icon: ''
        }
    },
    {
        path: '/product/productList/create/create',
        name: 'productList3',
        component: () => import('@/views/product/productList/create/create'),
        hidden: true,
        meta: {
            title: '生成产品',
            icon: ''
        }
    },
    {
        path: '/product/productList/edit/index',
        name: 'productList4',
        component: () => import('@/views/product/productList/edit/index'),
        hidden: true,
        meta: {
            title: '修改产品',
            icon: ''
        }
    },
    {
        path: '/product/productList/edit/create',
        name: 'productList5',
        component: () => import('@/views/product/productList/edit/create'),
        hidden: true,
        meta: {
            title: '生成产品',
            icon: ''
        }
    },
    {
        path: '/product/productList/proInfo/index',
        name: 'productList6',
        component: () => import('@/views/product/productList/proInfo/index'),
        hidden: true,
        meta: {
            title: '产品详情',
            icon: ''
        }
    },
    {
        path: '/product/productList/proInfo/create',
        name: 'productList7',
        component: () => import('@/views/product/productList/proInfo/create'),
        hidden: true,
        meta: {
            title: '添加到销售单',
            icon: ''
        }
    },
    {
        path: '/product/productList/bind/SKU',
        name: 'productList8',
        component: () => import('@/views/product/productList/bind/SKU'),
        hidden: true,
        meta: {
            title: 'SKU',
            icon: ''
        }
    },
    {
        path: '/product/productList/bind/SKUXQ',
        name: 'productList9',
        component: () => import('@/views/product/productList/bind/SKUXQ'),
        hidden: true,
        meta: {
            title: 'SKU详情',
            icon: ''
        }
    },
    {
        path: '/product/productList/bind/material',
        name: 'productList10',
        component: () => import('@/views/product/productList/bind/material'),
        hidden: true,
        meta: {
            title: '绑定用料',
            icon: ''
        }
    },
    {
        path: '/product/productList/bind/process',
        name: 'productList11',
        component: () => import('@/views/product/productList/bind/process'),
        hidden: true,
        meta: {
            title: '绑定工艺',
            icon: ''
        }
    },
    {
        path: '/product/productList/bind/produce',
        name: 'productList12',
        component: () => import('@/views/product/productList/bind/produce'),
        hidden: true,
        meta: {
            title: '生产模板列表',
            icon: ''
        }
    },
    {
        path: '/product/prospec/index',
        name: 'prospec1',
        component: () => import('@/views/product/prospec/index'),
        meta: {
            title: '产品规格',
            icon: ''
        }
    },
    {
        path: '/product/prospec/create',
        name: 'prospec2',
        component: () => import('@/views/product/prospec/create'),
        hidden: true,
        meta: {
            title: '添加规格',
            icon: ''
        }
    },
    {
        path: '/product/prospec/edit',
        name: 'prospec3',
        component: () => import('@/views/product/prospec/edit'),
        hidden: true,
        meta: {
            title: '修改规格',
            icon: ''
        }
    },
    {
        path: '/product/processList/index',
        name: 'processList1',
        component: () => import('@/views/product/processList/index'),
        meta: {
            title: '工艺单列表',
            icon: ''
        }
    },
    {
        path: '/product/processList/create',
        name: 'processList2',
        component: () => import('@/views/product/processList/create'),
        hidden: true,
        meta: {
            title: '添加工艺单',
            icon: ''
        }
    },
    {
        path: '/product/processList/edit',
        name: 'processList3',
        component: () => import('@/views/product/processList/edit'),
        hidden: true,
        meta: {
            title: '修改工艺单',
            icon: ''
        }
    },
    {
        path: '/product/materialList/index',
        name: 'materialList3',
        component: () => import('@/views/product/materialList/index'),
        meta: {
            title: '用料单列表',
            icon: ''
        }
    },
    {
        path: '/product/materialList/create',
        name: 'materialList4',
        component: () => import('@/views/product/materialList/create'),
        hidden: true,
        meta: {
            title: '添加用料单',
            icon: ''
        }
    },
    {
        path: '/product/materialList/edit',
        name: 'materialList5',
        component: () => import('@/views/product/materialList/edit'),
        hidden: true,
        meta: {
            title: '修改用料单',
            icon: ''
        }
    },
    {
        path: '/product/produceList/index',
        name: 'produceList1',
        component: () => import('@/views/product/produceList/index'),
        meta: {
            title: '生产模板列表',
            icon: ''
        }
    },
    {
        path: '/product/produceList/create',
        name: 'produceList2',
        component: () => import('@/views/product/produceList/create'),
        hidden: true,
        meta: {
            title: '添加生产模板列表',
            icon: ''
        }
    },
    {
        path: '/product/produceList/edit',
        name: 'produceList3',
        component: () => import('@/views/product/produceList/edit'),
        hidden: true,
        meta: {
            title: '修改生产模板列表',
            icon: ''
        }
    },
    {
        path: '/factory/instruct/index',
        name: 'Instruct5',
        component: () => import('@/views/factory/instruct/index'),
        meta: {
            title: '使用说明',
            icon: ''
        }
    }
    ]
},
{
    path: '/material',
    component: Layout,
    redirect: '/material/materialspec',
    name: 'material',
    meta: {
        title: '原材料',
        icon: ''
    },
    children: [{
        path: '/material/materialspec/index',
        name: 'materialspec1',
        component: () => import('@/views/material/materialspec/index'),
        meta: {
            title: '原材料规格',
            icon: ''
        }
    },
    {
        path: '/material/materialspec/create',
        name: 'materialspec2',
        component: () => import('@/views/material/materialspec/create'),
        hidden: true,
        meta: {
            title: '添加规格',
            icon: ''
        }
    },
    {
        path: '/material/materialspec/edit',
        name: 'materialspec3',
        component: () => import('@/views/material/materialspec/edit'),
        hidden: true,
        meta: {
            title: '修改规格',
            icon: ''
        }
    },
    {
        path: '/material/materialList/index',
        name: 'materialList1',
        component: () => import('@/views/material/materialList/index'),
        meta: {
            title: '原材料列表',
            icon: ''
        }
    },
    {
        path: '/material/materialList/create',
        name: 'materialList2',
        component: () => import('@/views/material/materialList/create'),
        hidden: true,
        meta: {
            title: '添加原材料',
            icon: ''
        }
    },
    {
        path: '/material/materialList/edit',
        name: 'materialList6',
        component: () => import('@/views/material/materialList/edit'),
        hidden: true,
        meta: {
            title: '修改原材料',
            icon: ''
        }
    },
    {
        path: '/material/detailList/index',
        name: 'detailList1',
        component: () => import('@/views/material/detailList/index'),
        meta: {
            title: '原材料明细表',
            icon: ''
        }
    },
    {
        path: '/material/detailList/create',
        name: 'detailList2',
        component: () => import('@/views/material/detailList/create'),
        hidden: true,
        meta: {
            title: '添加原料单',
            icon: ''
        }
    },
    {
        path: '/material/detailList/edit',
        name: 'detailList3',
        component: () => import('@/views/material/detailList/edit'),
        hidden: true,
        meta: {
            title: '修改原料单',
            icon: ''
        }
    },
    {
        path: '/material/detailList/distribute',
        name: 'detailList4',
        component: () => import('@/views/material/detailList/distribute'),
        hidden: true,
        meta: {
            title: '打印原材料配货单',
            icon: ''
        }
    },
    {
        path: '/material/detailList/stock',
        name: 'detailList5',
        component: () => import('@/views/material/detailList/stock'),
        hidden: true,
        meta: {
            title: '打印原材料盘存单',
            icon: ''
        }
    },
    {
        path: '/material/detailList/storage',
        name: 'detailList6',
        component: () => import('@/views/material/detailList/storage'),
        hidden: true,
        meta: {
            title: '打印原材料入库单',
            icon: ''
        }
    },
    {
        path: '/material/detailList/machining',
        name: 'detailList7',
        component: () => import('@/views/material/detailList/machining'),
        hidden: true,
        meta: {
            title: '打印外发加工单',
            icon: ''
        }
    },
    {
        path: '/material/detailPlan/index',
        name: 'detailPlan1',
        component: () => import('@/views/material/detailPlan/index'),
        meta: {
            title: '原材料计划',
            icon: ''
        }
    },
    {
        path: '/material/detailPlan/create',
        name: 'detailPlan2',
        component: () => import('@/views/material/detailPlan/create'),
        hidden: true,
        meta: {
            title: '添加原材料计划',
            icon: ''
        }
    },
    {
        path: '/material/detailPlan/edit',
        name: 'detailPlan3',
        component: () => import('@/views/material/detailPlan/edit'),
        hidden: true,
        meta: {
            title: '修改原材料计划',
            icon: ''
        }
    },
    {
        path: '/material/instruct/index',
        name: 'Instruct6',
        component: () => import('@/views/material/instruct/index'),
        meta: {
            title: '使用说明',
            icon: ''
        }
    }
    ]
},
{
    path: '/detailreport',
    component: Layout,
    redirect: '/detailreport/produce',
    name: 'detailreport',
    meta: {
        title: '明细报表',
        icon: ''
    },
    children: [
        {
            path: '/detailreport/produce/index',
            name: 'produce1',
            component: () => import('@/views/detailreport/produce/index'),
            meta: {
                title: '生产计划表',
                icon: ''
            }
        },
        {
            path: '/detailreport/produce/mingXi',
            name: 'produce_ming_xi',
            component: () => import('@/views/detailreport/produce/mingXi'),
            meta: {
                title: '生产计划表明细',
                icon: ''
            }
        },
        {
            path: '/detailreport/produce/create',
            name: 'produce2',
            component: () => import('@/views/detailreport/produce/create'),
            hidden: true,
            meta: {
                title: '添加生产计划表',
                icon: ''
            }
        },
        {
            path: '/detailreport/produce/edit',
            name: 'produce3',
            component: () => import('@/views/detailreport/produce/edit'),
            hidden: true,
            meta: {
                title: '修改生产计划表',
                icon: ''
            }
        },
        {
            path: '/detailreport/produce/planform',
            name: 'produce4',
            component: () => import('@/views/detailreport/produce/planform'),
            hidden: true,
            meta: {
                title: '生产计划单',
                icon: ''
            }
        },
        {
            path: '/detailreport/tailor/index',
            name: 'tailor1',
            component: () => import('@/views/detailreport/tailor/index'),
            meta: {
                title: '裁剪明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/tailor/create',
            name: 'tailor2',
            component: () => import('@/views/detailreport/tailor/create'),
            hidden: true,
            meta: {
                title: '添加裁剪明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/tailor/edit',
            name: 'tailor3',
            component: () => import('@/views/detailreport/tailor/edit'),
            hidden: true,
            meta: {
                title: '修改裁剪明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/tailor/tailorform',
            name: 'tailor4',
            component: () => import('@/views/detailreport/tailor/tailorform'),
            hidden: true,
            meta: {
                title: '裁剪明细单',
                icon: ''
            }
        },
        {
            path: '/detailreport/machine/index',
            name: 'machine1',
            component: () => import('@/views/detailreport/machine/index'),
            meta: {
                title: '加工明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/machine/create',
            name: 'machine2',
            component: () => import('@/views/detailreport/machine/create'),
            hidden: true,
            meta: {
                title: '添加加工明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/machine/edit',
            name: 'machine3',
            component: () => import('@/views/detailreport/machine/edit'),
            hidden: true,
            meta: {
                title: '修改加工明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/machine/machineform',
            name: 'machine4',
            component: () => import('@/views/detailreport/machine/machineform'),
            hidden: true,
            meta: {
                title: '外发加工单',
                icon: ''
            }
        },
        {
            path: '/detailreport/machine/detailform',
            name: 'machine5',
            component: () => import('@/views/detailreport/machine/detailform'),
            hidden: true,
            meta: {
                title: '加工入库明细单',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/index',
            name: 'product1',
            component: () => import('@/views/detailreport/product/index'),
            meta: {
                title: '成品明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/create',
            name: 'product2',
            component: () => import('@/views/detailreport/product/create'),
            hidden: true,
            meta: {
                title: '添加成品明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/edit',
            name: 'product3',
            component: () => import('@/views/detailreport/product/edit'),
            hidden: true,
            meta: {
                title: '修改成品明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/inventory',
            name: 'product4',
            component: () => import('@/views/detailreport/product/inventory'),
            hidden: true,
            meta: {
                title: '成品盘存单',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/transfer',
            name: 'product5',
            component: () => import('@/views/detailreport/product/transfer'),
            hidden: true,
            meta: {
                title: '成品仓库转移单',
                icon: ''
            }
        },
        {
            path: '/detailreport/product/entryform',
            name: 'product6',
            component: () => import('@/views/detailreport/product/entryform'),
            hidden: true,
            meta: {
                title: '成品半成品入库单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/index',
            name: 'sales1',
            component: () => import('@/views/detailreport/sales/index'),
            meta: {
                title: '销售明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/create',
            name: 'sales2',
            component: () => import('@/views/detailreport/sales/create'),
            hidden: true,
            meta: {
                title: '添加销售单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/edit1',
            name: 'sales3',
            component: () => import('@/views/detailreport/sales/edit1'),
            hidden: true,
            meta: {
                title: '修改销售单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/deliverInfo',
            name: 'sales4',
            component: () => import('@/views/detailreport/sales/deliverInfo'),
            hidden: true,
            meta: {
                title: '库存不足详情', // 原来是 XXXX发货详情
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/distribute',
            name: 'sales5',
            component: () => import('@/views/detailreport/sales/distribute'),
            hidden: true,
            meta: {
                title: '打印配货单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/reviewOrder',
            name: 'sales6',
            component: () => import('@/views/detailreport/sales/reviewOrder'),
            hidden: true,
            meta: {
                title: '订单详情审核',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/collectInfo',
            name: 'sales7',
            component: () => import('@/views/detailreport/sales/collectInfo'),
            hidden: true,
            meta: {
                title: '收集数据订单详情',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/returnInfo',
            name: 'sales8',
            component: () => import('@/views/detailreport/sales/returnInfo'),
            hidden: true,
            meta: {
                title: '退货单详情',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/returnExamine',
            name: 'sales9',
            component: () => import('@/views/detailreport/sales/returnExamine'),
            hidden: true,
            meta: {
                title: '退货单审核',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/addOrder1',
            name: 'sales10',
            component: () => import('@/views/detailreport/sales/addOrder1'),
            hidden: true,
            meta: {
                title: '常规下单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/addOrder2',
            name: 'sales11',
            component: () => import('@/views/detailreport/sales/addOrder2'),
            hidden: true,
            meta: {
                title: '收集数据下单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/addOrder3',
            name: 'sales12',
            component: () => import('@/views/detailreport/sales/addOrder3'),
            hidden: true,
            meta: {
                title: '收集数据代收款下单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/edit2',
            name: 'sales13',
            component: () => import('@/views/detailreport/sales/edit2'),
            hidden: true,
            meta: {
                title: '修改销售单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/edit3',
            name: 'sales14',
            component: () => import('@/views/detailreport/sales/edit3'),
            hidden: true,
            meta: {
                title: '修改销售单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/statistics',
            name: 'sales15',
            component: () => import('@/views/detailreport/sales/statistics'),
            hidden: true,
            meta: {
                title: '统计',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/invoice',
            name: 'sales16',
            component: () => import('@/views/detailreport/sales/invoice'),
            hidden: true,
            meta: {
                title: '班级发货清单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/assignGoods',
            name: 'sales17',
            component: () => import('@/views/detailreport/sales/assignGoods'),
            hidden: true,
            meta: {
                title: '打印配货单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/expressOrder',
            name: 'sales18',
            component: () => import('@/views/detailreport/sales/expressOrder'),
            hidden: true,
            meta: {
                title: '打印快递单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/drawOrder',
            name: 'sales19',
            component: () => import('@/views/detailreport/sales/drawOrder'),
            hidden: true,
            meta: {
                title: '打印有图清单',
                icon: ''
            }
        },
        {
            path: '/detailreport/sales/tripletOrder',
            name: 'sales20',
            component: () => import('@/views/detailreport/sales/tripletOrder'),
            hidden: true,
            meta: {
                title: '打印发货三联单',
                icon: ''
            }
        },
        {
            path: '/detailreport/finance/index',
            name: 'finance1',
            component: () => import('@/views/detailreport/finance/index'),
            meta: {
                title: '财务明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/finance/recharge',
            name: 'finance2',
            component: () => import('@/views/detailreport/finance/recharge'),
            hidden: true,
            meta: {
                title: '会员预存款',
                icon: ''
            }
        },
        {
            path: '/detailreport/logos/index',
            name: 'logos',
            component: () => import('@/views/detailreport/logos/index'),
            meta: {
                title: '校标明细表',
                icon: ''
            }
        },
        {
            path: '/detailreport/collect/index',
            name: 'collect',
            component: () => import('@/views/detailreport/collect/index'),
            meta: {
                title: '收集数据明细',
                icon: ''
            }
        },
        {
            path: '/detailreport/collect/info',
            name: 'info',
            component: () => import('@/views/detailreport/collect/info'),
            hidden: true,
            meta: {
                title: '数据详情',
                icon: ''
            }
        },
        {
            path: '/detailreport/instruct/index',
            name: 'Instruct7',
            component: () => import('@/views/detailreport/instruct/index'),
            meta: {
                title: '使用说明',
                icon: ''
            }
        }
    ]
},
{
    path: '/scancode',
    component: Layout,
    redirect: '/scancode/index',
    name: 'outscancode',
    meta: {
        title: '扫码发货',
        icon: ''
    },
    children: [{
        path: 'index',
        name: 'outScancode',
        component: () => import('@/views/scancode/index'),
        meta: {
            title: '扫码发货',
            icon: ''
        }
    }]
},
{
    path: '/vote',
    component: Layout,
    redirect: '/vote/index',
    name: 'outvote',
    meta: {
        title: '投票',
        icon: ''
    },
    children: [{
        path: '/vote/index',
        name: 'outVote1',
        component: () => import('@/views/vote/index'),
        meta: {
            title: '投票',
            icon: ''
        }
    }, {
        path: '/vote/create',
        name: 'outVote2',
        component: () => import('@/views/vote/create'),
        meta: {
            title: '增加投票',
            icon: ''
        }
    }]
},

{
    path: '/backlog',
    component: Layout,
    redirect: '/backlog/index',
    name: 'backlog',
    meta: {
        title: '待办事项',
        icon: ''
    },
    children: [{
        path: 'index',
        name: 'backlog1',
        component: () => import('@/views/backlog/index'),
        meta: {
            title: '待办事项',
            icon: ''
        }
    }]
},

// 以下是模板
{
    path: '/documentation',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/documentation/index'),
        name: 'Documentation',
        meta: {
            title: 'documentation',
            icon: 'documentation'
        } // , affix: true
    }]
},
{
    path: '/guide',
    component: Layout,
    redirect: '/guide/index',
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/guide/index'),
        name: 'Guide',
        meta: {
            title: 'guide',
            icon: 'guide',
            noCache: true
        }
    }]
},
{
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: {
            title: 'profile',
            icon: 'user',
            noCache: true
        }
    }]
}
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [{
    path: '/permission',
    component: Layout,
    redirect: '/permission/page',
    alwaysShow: true, // will always show the root menu
    name: 'Permission',
    hidden: true,
    meta: {
        title: 'permission',
        icon: 'lock',
        roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [{
        path: 'page',
        component: () => import('@/views/permission/page'),
        name: 'PagePermission',
        meta: {
            title: 'pagePermission',
            roles: ['admin'] // or you can only set roles in sub nav
        }
    },
    {
        path: 'directive',
        component: () => import('@/views/permission/directive'),
        name: 'DirectivePermission',
        meta: {
            title: 'directivePermission'
            // if do not set roles, means: this page does not require permission
        }
    },
    {
        path: 'role',
        component: () => import('@/views/permission/role'),
        name: 'RolePermission',
        meta: {
            title: 'rolePermission',
            roles: ['admin']
        }
    }
    ]
},

{
    path: '/icon',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/icons/index'),
        name: 'Icons',
        meta: {
            title: 'icons',
            icon: 'icon',
            noCache: true
        }
    }]
},

    /** when your routing map is too long, you can split it into small modules **/
    // componentsRouter,
    // chartsRouter,
    // nestedRouter,
    tableRouter,

{
    path: '/example',
    component: Layout,
    redirect: '/example/list',
    name: 'Example',
    hidden: true,
    meta: {
        title: 'example',
        icon: 'el-icon-s-help'
    },
    children: [{
        path: 'create',
        component: () => import('@/views/example/create'),
        name: 'CreateArticle',
        meta: {
            title: 'createArticle',
            icon: 'edit'
        }
    },
    {
        path: 'edit/:id(\\d+)',
        component: () => import('@/views/example/edit'),
        name: 'EditArticle',
        meta: {
            title: 'editArticle',
            noCache: true,
            activeMenu: '/example/list'
        },
        hidden: true
    },
    {
        path: 'list',
        component: () => import('@/views/example/list'),
        name: 'ArticleList',
        meta: {
            title: 'articleList',
            icon: 'list'
        }
    }
    ]
},

{
    path: '/tab',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/tab/index'),
        name: 'Tab',
        meta: {
            title: 'tab',
            icon: 'tab'
        }
    }]
},

{
    path: '/error',
    component: Layout,
    redirect: 'noRedirect',
    name: 'ErrorPages',
    hidden: true,
    meta: {
        title: 'errorPages',
        icon: '404'
    },
    children: [{
        path: '401',
        component: () => import('@/views/error-page/401'),
        name: 'Page401',
        meta: {
            title: 'page401',
            noCache: true
        }
    },
    {
        path: '404',
        component: () => import('@/views/error-page/404'),
        name: 'Page404',
        meta: {
            title: 'page404',
            noCache: true
        }
    }
    ]
},

{
    path: '/error-log',
    component: Layout,
    hidden: true,
    children: [{
        path: 'log',
        component: () => import('@/views/error-log/index'),
        name: 'ErrorLog',
        meta: {
            title: 'errorLog',
            icon: 'bug'
        }
    }]
},

{
    path: '/excel',
    component: Layout,
    redirect: '/excel/export-excel',
    name: 'Excel',
    hidden: true,
    meta: {
        title: 'excel',
        icon: 'excel'
    },
    children: [{
        path: 'export-excel',
        component: () => import('@/views/excel/export-excel'),
        name: 'ExportExcel',
        meta: {
            title: 'exportExcel'
        }
    },
    {
        path: 'export-selected-excel',
        component: () => import('@/views/excel/select-excel'),
        name: 'SelectExcel',
        meta: {
            title: 'selectExcel'
        }
    },
    {
        path: 'export-merge-header',
        component: () => import('@/views/excel/merge-header'),
        name: 'MergeHeader',
        meta: {
            title: 'mergeHeader'
        }
    },
    {
        path: 'upload-excel',
        component: () => import('@/views/excel/upload-excel'),
        name: 'UploadExcel',
        meta: {
            title: 'uploadExcel'
        }
    }
    ]
},

{
    path: '/zip',
    component: Layout,
    redirect: '/zip/download',
    alwaysShow: true,
    hidden: true,
    name: 'Zip',
    meta: {
        title: 'zip',
        icon: 'zip'
    },
    children: [{
        path: 'download',
        component: () => import('@/views/zip/index'),
        name: 'ExportZip',
        meta: {
            title: 'exportZip'
        }
    }]
},

{
    path: '/pdf',
    component: Layout,
    redirect: '/pdf/index',
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/pdf/index'),
        name: 'PDF',
        meta: {
            title: 'pdf',
            icon: 'pdf'
        }
    }]
},
{
    path: '/pdf/download',
    component: () => import('@/views/pdf/download'),
    hidden: true
},

{
    path: '/theme',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/theme/index'),
        name: 'Theme',
        meta: {
            title: 'theme',
            icon: 'theme'
        }
    }]
},

{
    path: '/clipboard',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/clipboard/index'),
        name: 'ClipboardDemo',
        meta: {
            title: 'clipboardDemo',
            icon: 'clipboard'
        }
    }]
},

{
    path: '/i18n',
    component: Layout,
    hidden: true,
    children: [{
        path: 'index',
        component: () => import('@/views/i18n-demo/index'),
        name: 'I18n',
        meta: {
            title: 'i18n',
            icon: 'international'
        }
    }]
},

{
    path: 'external-link',
    component: Layout,
    hidden: true,
    children: [{
        path: 'https://github.com/PanJiaChen/vue-element-admin',
        meta: {
            title: 'externalLink',
            icon: 'link'
        }
    }]
},

// 404 page must be placed at the end !!!
{
    path: '*',
    redirect: '/404',
    hidden: true
}
]

const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router
