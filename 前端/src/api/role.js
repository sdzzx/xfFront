import request from '@/utils/request'

export function getRoutes() {
    return request({
        url: '/blade-system/menu/list',
        method: 'get'
    })
}

export function getRoles() {
    return request({
        url: '/vue-element-admin/roles',
        method: 'get'
    })
}

export function addRole(data) {
    return request({
        url: '/vue-element-admin/role',
        method: 'post',
        data
    })
}

export function updateRole(id, data) {
    return request({
        url: `/vue-element-admin/role/${id}`,
        method: 'put',
        data
    })
}

export function deleteRole(id) {
    return request({
        url: `/vue-element-admin/role/${id}`,
        method: 'delete'
    })
}

export function roleTree() {
    return request({
        url: '/blade-system/role/tree',
        method: 'get'
    })
}

export function roleAllTree() {
    return request({
        url: '/blade-system/role/treeList',
        method: 'get'
    })
}
export function roleList() {
    return request({
        url: '/blade-system/role/list',
        method: 'get'
    })
}
export function userList(data) {
    return request({
        url: '/blade-user/user-list',
        method: 'get',
        params: data
    })
}

