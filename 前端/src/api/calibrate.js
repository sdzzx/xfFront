import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfmanufacturingshop/listXB',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/xfmanufacturingshop/submitXB',
        method: 'post',
        data: data
    })
}

export function saveData(data) {
    return request({
        url: '/blade-business/xfmanufacturingshop/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/xfmanufacturingshop/removeXB',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfmanufacturingshop/excelExportXB',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function ckpageList(data) {
    return request({
        url: '/blade-business/storehouse/pageList',
        method: 'get',
        params: data
    })
}
