import request from '@/utils/request'

export function lazyTree(params) {
    return request({
        url: '/blade-system/region/lazy-tree',
        method: 'get',
        params: params
    })
}
