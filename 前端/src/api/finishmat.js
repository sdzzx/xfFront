import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/materialstoreshift/page',
        method: 'get',
        params: data
    })
}
export function getList1(data) {
    return request({
        url: '/blade-business/materialstoreshift/getList',
        method: 'get',
        params: data
    })
}
export function editData(data) {
    return request({
        url: '/blade-business/materialstoreshift/submit',
        method: 'post',
        data: data
    })
}

export function updateData(data) {
    return request({
        url: '/blade-business/materialstoreshift/update',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/materialstoreshift/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/materialstoreshift/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getTypeData(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getYCLData(data) {
    return request({
        url: '/blade-business/xfmaterial/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getCKData(data) {
    return request({
        url: '/blade-business/storehouse/pageList',
        method: 'get',
        params: data
    })
}

export function getYCLList(data) {
    return request({
        url: '/blade-business/materialstoreshift/yclckzydetail',
        method: 'get',
        params: data
    })
}
