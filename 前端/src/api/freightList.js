import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfcarriage/list',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/xfcarriage/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/xfcarriage/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfcarriage/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function wlpageList(data) {
    return request({
        url: '/blade-business/xflogistics/pageList',
        method: 'get',
        params: data
    })
}

export function lazyTree(data) {
    return request({
        url: '/blade-business/xfcarriage/lazy-tree',
        method: 'get',
        params: data
    })
}
