import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfproduct/page',
        method: 'get',
        params: data
    })
}

export function getListList(data) {
    return request({
        url: '/blade-business/xfproduct/getList',
        method: 'get',
        params: data
    })
}

export function saveData(data) {
    return request({
        url: '/blade-business/xfproduct/save',
        method: 'post',
        data: data
    })
}
export function editData(data) {
    return request({
        url: '/blade-business/xfproduct/update',
        method: 'post',
        data: data
    })
}
export function removeData(data) {
    return request({
        url: '/blade-business/xfproduct/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfproduct/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getMaterial(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/getList',
        method: 'get',
        params: data
    })
}

export function addxfwashing(data) {
    return request({
        url: '/blade-business/xfwashing/save',
        method: 'post',
        data: data
    })
}

export function getxfwashing(data) {
    return request({
        url: '/blade-business/xfwashing/getList',
        method: 'get',
        params: data
    })
}
export function getphy(data) {
    return request({
        url: '/blade-user/user-list',
        method: 'get',
        params: data
    })
}
export function getcpgg(data) {
    return request({
        url: '/blade-business/xfproductstandard/getList',
        method: 'get',
        params: data
    })
}
export function getck(data) {
    return request({
        url: '/blade-business/storehouse/pageList',
        method: 'get',
        params: data
    })
}
export function getsku(data) {
    return request({
        url: '/blade-business/productsku/page',
        method: 'get',
        params: data
    })
}
export function editsku(data) {
    return request({
        url: '/blade-business/productsku/save',
        method: 'post',
        data: data
    })
}
export function gethy(data) {
    return request({
        url: '/blade-user/getList',
        method: 'get',
        params: data
    })
}
export function getxb(data) {
    return request({
        url: '/blade-business/xfmanufacturingshop/getList',
        method: 'get',
        params: data
    })
}

export function getwl(data) {
    return request({
        url: '/blade-business/xflogistics/pageList',
        method: 'get',
        params: data
    })
}

export function getLazyTree(data) {
    return request({
        url: '/blade-system/region/lazy-tree',
        method: 'get',
        params: data
    })
}

export function getGY(data) {
    return request({
        url: '/blade-business/technology/getList',
        method: 'get',
        params: data
    })
}

export function getSchool(data) {
    return request({
        url: '/blade-user/user/listSelect',
        method: 'get',
        params: data
    })
}

export function addXSD(data) {
    return request({
        url: '/blade-business/salesticket/save ',
        method: 'post',
        data: data
    })
}