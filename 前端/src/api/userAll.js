import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-user/pageList',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-user/submit',
        method: 'post',
        params: data
    })
}


export function updateUser(data) {
    return request({
        url: '/blade-user/updateUser',
        method: 'post',
        params: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-user/remove',
        method: 'post',
        params: data
    })
}
export function resetPwd(data) {
    return request({
        url: '/blade-user/reset-password',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-user/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function lazyTree(data) {
    return request({
        url: '/blade-system/region/lazy-tree',
        method: 'get',
        params: data
    })
}
export function ckpageList(data) {
    return request({
        url: '/blade-business/storehouse/pageList',
        method: 'get',
        params: data
    })
}
export function rolePageList(data) {
    return request({
        url: '/blade-system/role/pageList',
        method: 'get',
        params: data
    })
}
