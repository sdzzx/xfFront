import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/storehouse/page',
        method: 'get',
        params: data
    })
}
export function editData(data) {
    return request({
        url: '/blade-business/storehouse/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/storehouse/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/storehouse/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getGLZ(data) {
    return request({
        url: '/blade-system/role/tree ',
        method: 'get',
        params: data
    })
}
export function getGLY(data) {
    return request({
        url: '/blade-user/user-list',
        method: 'get',
        params: data
    })
}