import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/technology/page',
        method: 'get',
        params: data
    })
}

export function saveData(data) {
    return request({
        url: '/blade-business/technology/save',
        method: 'post',
        data: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/technology/update',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/technology/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/technology/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getProduct(data) {
    return request({
        url: '/blade-business/xfproduct/getList',
        method: 'get',
        params: data
    })
}
