import request from '@/utils/request'

export function sales_addOrder1(data) {
    return request({
        url: '/blade-business/salesticket/save',
        method: 'post',
        data: data
    })
}
