import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/productiontemplate/page',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/productiontemplate/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/productiontemplate/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/productiontemplate/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getProduct(data) {
    return request({
        url: '/blade-business/xfproduct/getList',
        method: 'get',
        params: data
    })
}

export function getdict(data) {
    return request({
        url: '/blade-system/dict/dictTemplateList',
        method: 'get',
        params: data
    })
}
