import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-tailor/page',
        method: 'get',
        params: data
    })
}

export function getList_2(data) {
    return request({
        url: '/blade-business/productioncut/page',
        method: 'get',
        params: data
    })
}

export function getCutList(data) {
    return request({
        url: '/blade-business/productioncut/cutList',
        method: 'get',
        params: data
    })
}

export function addOreditData(data) {
    return request({
        url: '/blade-business/productionplan/submit',
        method: 'post',
        data: data
    })
}

export function addOreditData_2(data) {
    return request({
        url: '/blade-business/productioncut/submit',
        method: 'post',
        data: data
    })
}

export function getCutLoge(data) {
    return request({
        url: '/blade-business/logdetailreport/getCutLoge',
        method: 'post',
        data: data
    })
}

export function getCaiJianDanHaoBiao(data) {
    return request({
        url: '/blade-business/productioncut/getCutList',
        method: 'get',
        data: data
    })
}

export function getJiaGongRuKuBiao(data) {
    return request({
        url: '/blade-business/processingschedule/getTypeOne',
        method: 'get',
        data: data
    })
}

export function getFanHuiFanGongBiao(data) {
    return request({
        url: '/blade-business/processingschedule/getTypeTwo',
        method: 'get',
        data: data
    })
}

export function getFanGongRuKuBiao(data) {
    a(data, 'getFanGongRuKuBiao data ------------------------------')
    return request({
        url: '/blade-business/processingschedule/getTypeThree',
        method: 'get',
        data: data
    })
}

// export function addMachineDetail(data) {
//     return request({
//         url: '/blade-business/processingschedule/submit',
//         method: 'post',
//         params: data
//     })
// }
