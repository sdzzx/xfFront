import request from '@/utils/request'

export function login(data) {
    return request({
        url: '/blade-auth/token',
        method: 'post',
        params: data
    })
}

export function getInfo(token) {
    return request({
        url: '/vue-element-admin/user/info',
        method: 'get',
        params: { token }
    })
}

export function logout() {
    return request({
        url: '/user/logout',
        method: 'get'
    })
}

export function sendValidate(data) {
    return request({
        url: '/sms/endpoint/send-validate',
        method: 'post',
        params: data
    })
}

export function forget(data) {
    return request({
	  url: '/blade-user/forget-password',
	  method: 'post',
        params: data
    })
}

export function getUserList(data) {
    return request({
	  url: 'blade-user/userList',
	  method: 'get',
        params: data
    })
}
