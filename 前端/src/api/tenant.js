import request from '@/utils/request'

export function getTenant(data) {
    return request({
        url: '/blade-system/tenant/select',
        method: 'get'
    })
}
