import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/billmaterials/page',
        method: 'get',
        params: data
    })
}
export function editData(data) {
    return request({
        url: '/blade-business/billmaterials/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/billmaterials/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/billmaterials/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getProduct(data) {
    return request({
        url: '/blade-business/xfproduct/getList',
        method: 'get',
        params: data
    })
}


export function getproductNames(data) {
    return request({
        url: '/blade-business/xfproduct/getproductName',
        method: 'get',
        params: data
    })
}

export function getAllMaterial(data) {
    return request({
        url: '/blade-business/xfmaterial/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getAllMaterial_2(data) {
    return request({
        url: 'blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getColorSexByid(data) {
    return request({
        url: '/blade-business/xfproduct/getColorSexById',
        method: 'get',
        params: data
    })
}
