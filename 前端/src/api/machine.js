import request from '@/utils/request'

export function addMachineDetail(data) {
    return request({
        url: '/blade-business/processingschedule/submit',
        method: 'post',
        data: data
    })
}
