import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/materialinventory/page',
        method: 'get',
        params: data
    })
}
export function getList1(data) {
    return request({
        url: '/blade-business/materialinventory/getList',
        method: 'get',
        params: data
    })
}
export function editData(data) {
    return request({
        url: '/blade-business/materialinventory/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/materialinventory/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/materialinventory/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}
export function getTypeData(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}
