import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfmaterial/page',
        method: 'get',
        params: data
    })
}
export function getMaterialList(data) {
    return request({
        url: '/blade-business/xfmaterial/list',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/xfmaterial/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/xfmaterial/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfmaterial/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getTypeData(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}
