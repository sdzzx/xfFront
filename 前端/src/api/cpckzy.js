import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-cpckzy/page',
        method: 'get',
        params: data
    })
}

export function getList1(data) {
    return request({
        url: '/blade-cpckzy/pageList',
        method: 'get',
        params: data
    })
}

export function addOreditData(data) {
    return request({
        url: '/blade-cpckzy/submit',
        method: 'post',
        data: data
    })
}

export function addGuige(data) {
    return request({
        url: '/blade-guige/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-cpckzy/remove',
        method: 'post',
        params: data
    })
}

export const excelExportCpckzys = (param) => { // 导出模板
    return request({
        url: '/blade-cpckzy/excelExportCpckzy',
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getCpList(data) {
    return request({
        url: '/blade-cpckzy/cpckzydetail',
        method: 'get',
        params: data
    })
}
