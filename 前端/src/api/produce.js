import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/productionplan/page',
        method: 'get',
        params: data
    })
}

export function getList_ming_xi(data) {
    return request({
        url: '/blade-business/productionplanrelevance/getPlanIdList',
        method: 'get',
        params: data
    })
}

export function getList_2(data) {
    return request({
        url: '/blade-business/xfproduct/getCutAddList',
        method: 'get',
        params: data
    })
}

export function addOreditData(data) {
    return request({
        url: '/blade-business/productionplan/submit',
        method: 'post',
        data: data
    })
}

export function getLogData(data) {
    return request({
        url: '/blade-business/logdetailreport/getList',
        method: 'get',
        data: data
    })
}

export function getLIstPage(data) {
    return request({
        url: '/blade-business/productionplanrelevance/page',
        method: 'get',
        data: data
    })
}
