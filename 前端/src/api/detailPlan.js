import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfmaterialplan/page',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/xfmaterialplan/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/xfmaterialplan/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfmaterialplan/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getTypeData(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getGYCData(data) {
    return request({
        url: '/blade-business/xfmaterialsupplier/pageList',
        method: 'get',
        params: data
    })
}

export function getYCLData(data) {
    return request({
        url: '/blade-business/xfmaterial/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getproductNames(data) {
    return request({
        url: '/blade-business/xfmaterialsupplier/getSupplierNname',
        method: 'get',
        params: data
    })
}
