import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-cppc/page',
        method: 'get',
        params: data
    })
}
export function getList1(data) {
    return request({
        url: '/blade-cppc/getList',
        method: 'get',
        params: data
    })
}

export function addCppcGuige(data) {
    return request({
        url: '/blade-cppc_guige/submit',
        method: 'post',
        data: data
    })
}

export function addOreditData(data) {
    return request({
        url: '/blade-cppc/submit',
        method: 'post',
        data: data
    })
}

export function removeData1(data) {
    return request({
        url: '/blade-cppc/remove',
        method: 'post',
        params: data
    })
}

export const excelExportcppcs = (param) => { // 导出模板
    return request({
        url: '/blade-cppc/excelExportcppc',
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getCppcLists(data) {
    return request({
        url: '/blade-cppc/cppcdetail',
        method: 'get',
        params: data
    })
}

