import request from '@/utils/request'

//校验手机号是否是当前会员绑定的手机号
export function checkPhone(data) {
    return request({
        url: '/blade-user/checkPhone',
        method: 'post',
        params: data
    })
}

//校验验证码
export function checkyzm(data) {
    return request({
        url: '/sms/endpoint/validate-message',
        method: 'post',
        params: data
    })
}


//校验验证码
export function resetpassword(data) {
    return request({
        url: '/blade-user/reset-password',
        method: 'post',
        params: data
    })
}

