import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/xfmaterialdetailed/page',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/xfmaterialdetailed/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/xfmaterialdetailed/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/xfmaterialdetailed/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function getTypeData(data) {
    return request({
        url: '/blade-business/xfmaterialspecifications/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getGYCData(data) {
    return request({
        url: '/blade-business/xfmaterialsupplier/pageList',
        method: 'get',
        params: data
    })
}

export function getYCLData(data) {
    return request({
        url: '/blade-business/xfmaterial/byMaterialType',
        method: 'get',
        params: data
    })
}

export function getPlanNoData(data) {
    return request({
        url: '/blade-business/xfmaterialplan/detailedByPlan',
        method: 'get',
        params: data
    })
}
