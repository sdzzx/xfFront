import request from '@/utils/request'

export function getList(data) {
    return request({
        url: '/blade-business/shipaddress/list',
        method: 'get',
        params: data
    })
}

export function editData(data) {
    return request({
        url: '/blade-business/shipaddress/submit',
        method: 'post',
        data: data
    })
}

export function removeData(data) {
    return request({
        url: '/blade-business/shipaddress/remove',
        method: 'post',
        params: data
    })
}

export const exportData = (param) => { // 导出模板
    return request({
        url: '/blade-business/shipaddress/excelExport',
        // headers: {
        // 	isToken: true,
        // 	'Authorization': 'Basic cGlnOnBpZw==',
        // 	'Content-Type': 'application/json'

        // },
        responseType: 'blob',
        method: 'get',
        params: param

    })
}

export function lazyTree(data) {
    return request({
        url: '/blade-system/region/lazy-tree',
        method: 'get',
        params: data
    })
}
