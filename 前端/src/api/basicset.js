import request from '@/utils/request'

export function detail(data) {
    return request({
        url: '/base/setting/detail',
        method: 'get'
    })
}

export function submit(data) {
    return request({
        url: '/base/setting/submit',
        method: 'post',
        data: data
    })
}
