import axios from 'axios'
import {
    MessageBox,
    Message
} from 'element-ui'
import store from '@/store'
import {
    getToken
} from '@/utils/auth'
import website from '@/utils/website'
import {
    Base64
} from 'js-base64'
// create an axios instance
const service = axios.create({
    // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
    // baseURL: 'http://eighttest.bjpuqin.com:8001/api/', // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 50000 // request timeout
})

// request interceptor
service.interceptors.request.use(
    config => {
        const meta = (config.meta || {})
        const isToken = meta.isToken === false
        config.headers['Authorization'] = `Basic ${Base64.encode(`${website.clientId}:${website.clientSecret}`)}`
        if (getToken() && !isToken) {
            config.headers['Blade-Auth'] = 'bearer ' +
                getToken() // 让每个请求携带token--['Authorization']为自定义key 请根据实际情况自行修改
        }
        // headers中配置serialize为true开启序列化
        if (config.method === 'post' && meta.isSerialize === true) {
            config.data = serialize(config.data)
        }
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    res => {
        const status = Number(res.status) || 200

        if (status === 401) {
            store.dispatch('FedLogOut').then(() => {
                router.push({
                    path: '/login'
                })
            })
            return
        }
        if (status == 400) {
            const message = res.data.msg || errorCode[status] || errorCode['default']
            Message({
                message: '您输入的内容格式错误，请重新输入正确格式',
                type: 'error'
            })
            return Promise.reject(new Error(message))
        }
        if (status !== 200 || res.data.code === 1) {
            const message = res.data.msg || errorCode[status] || errorCode['default']
            Message({
                message: message,
                type: 'error'
            })
            return Promise.reject(new Error(message))
        }

        return res.data
    },
    error => {
        console.log('err' + error) // for debug
        Message({
            message: error.response.data.msg,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)

export default service
