import {detail} from '@/api/basicset';

const state = {
  openhouse: '', // 是否开启仓库功能
}

const mutations = {
  // 修改仓库状态
  EDIT_OPEN_HOUSE: (state, data) => {
    state.openhouse = data
  }
}

const actions = {
  // 是否开启仓库功能
  editOpenHouse({commit}, data) {
    commit('EDIT_OPEN_HOUSE', data)
  },
  // 访问数据库基础设置
  getDataSetting({commit}) {
    detail().then(res => {
      commit('EDIT_OPEN_HOUSE', res.data.isOpenStore)
    });
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
