import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

import { Message } from 'element-ui'

const state = {
    token: getToken(),
    name: '',
    avatar: '',
    introduction: '',
    roles: [],
    user_data: ''
}

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
        state.introduction = introduction
    },
    SET_NAME: (state, name) => {
        state.name = name
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles
    },


    SET_USER_DATA: (state, data) => {
        state.user_data = data
    }
}

const actions = {
    // user login
    login({ commit }, userInfo) {
        return new Promise((resolve, reject) => {
            login(userInfo).then(response => {
                if (!response.success) {
                    Message.error(response.msg)
                    return
                }
                const { data } = response
                console.log(data)
                commit('SET_TOKEN', data.accessToken)
                commit('SET_USER_DATA', data.userList)
                sessionStorage.setItem("user_data", JSON.stringify(data.userList));
                console.log(sessionStorage.getItem('user_data'))
                sessionStorage.setItem('logo', data.logo)
                setToken(data.accessToken)
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // get user info
    getInfo({ commit, state }) {
        return new Promise((resolve, reject) => {
            // getInfo(state.token).then(response => {
            //   const { data } = response

            //   if (!data) {
            //     reject('Verification failed, please Login again.')
            //   }
            //   const { roles, name, avatar, introduction } = data

            //   // roles must be a non-empty array
            //   if (!roles || roles.length <= 0) {
            //     reject('getInfo: roles must be a non-null array!')
            //   }

            commit('SET_ROLES', ['admin', 'editor'])
            commit('SET_NAME', '')
            commit('SET_AVATAR', '')
            commit('SET_INTRODUCTION', '')
            resolve({ roles: ['admin', 'editor'] })
            // }).catch(error => {
            //   reject(error)
            // })
        })
    },

    // user logout
    logout({ commit, state, dispatch }) {
        return new Promise((resolve, reject) => {
            // logout(state.token).then(() => {
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resetRouter()

            // reset visited views and cached views
            // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
            dispatch('tagsView/delAllViews', null, { root: true })

            resolve()
            // }).catch(error => {
            //   reject(error)
            // })
        })
    },

    // remove token
    resetToken({ commit }) {
        return new Promise(resolve => {
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resolve()
        })
    },

    // dynamically modify permissions
    async changeRoles({ commit, dispatch }, role) {
        const token = role + '-token'

        commit('SET_TOKEN', token)
        setToken(token)

        const { roles } = await dispatch('getInfo')

        resetRouter()

        // generate accessible routes map based on roles
        const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })
        // dynamically add accessible routes
        router.addRoutes(accessRoutes)

        // reset visited views and cached views
        dispatch('tagsView/delAllViews', null, { root: true })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
